from PyQt5 import uic
from PyQt5.QtCore import pyqtSignal, QObject, QThread
from PyQt5.QtWidgets import QApplication, QMainWindow
from sys import argv, exit

class Calculator(QObject):
    """
    Der Calculator beinhält alle mathematischen Operationen
    """
    send_labeltext = pyqtSignal(str)
    send_labeltext_klein = pyqtSignal(str)
    
    def __init__(self):
        """
        Diese Funktion initialisiert die Klasse und setzt alle Startwerte des Calculators
        """

        super().__init__()
        self.newNumber = 0
        self.decimal = False

    
    def numbers(self):

        # Value bekommt den Wert der im Jeweiligen Button angezeigt wird. int() wandelt den String in eine Zahl um
        value = int(self.sender().text())

        if self.decimal == False:
            # der neue Wert wird rechts an den vorherigen Wert von newNumber angehängt
            self.newNumber = self.newNumber * 10 + value
        else:
            if "." in str(self.newNumber):
                self.newNumber = self.newNumber + value / (10 ** (len(str(self.newNumber)) - str(self.newNumber).index(".")))
                self.newNumber = round(self.newNumber, 8)
                
            else:
                self.newNumber = self.newNumber + value / 10

        self.send_labeltext.emit(str(self.newNumber))


    def operations(self):

        self.oldNumber = self.newNumber

        self.operation = self.sender().text()
        self.newNumber = 0
        self.decimal = False

        self.send_labeltext.emit(str(self.newNumber))
        self.send_labeltext_klein.emit(str(self.oldNumber) + " " + self.operation)


    def equal(self):

        if self.operation == "+":
            result = self.newNumber + self.oldNumber
        elif self.operation == "-":
            result = self.oldNumber - self.newNumber
        elif self.operation == "*":
            result = self.oldNumber * self.newNumber
        elif self.operation == "/":
            result = self.oldNumber / self.newNumber
        else:
            result = self.newNumber

        result = round(result, 8)

        self.send_labeltext.emit(str(result))
        self.send_labeltext_klein.emit(str(self.oldNumber) + " " + self.operation + " " + str(self.newNumber))
        self.newNumber = result
        self.decimal = False


    def c(self):

        self.newNumber = 0
        self.oldNumber = 0
        self.operation = None
        self.decimal = False

        self.send_labeltext_klein.emit("")
        self.send_labeltext.emit(str(self.newNumber))

    def ce(self):

        self.newNumber = 0
        self.decimal = False

        self.send_labeltext.emit(str(self.newNumber))


    def setDecimal(self):

        self.decimal = True

class AppWindow(QMainWindow):

    def __init__(self):
        """
        Diese Funktion erstellt das Hauptfenster und erstellt sämtliche Startwerte des Fensters
        """
        super().__init__()
        self.ui = uic.loadUi("calc.ui", self)
        self.ui.show()

        self.calculator = Calculator()

        self.setupThreading()

        # Buttons mit Funktionen verbinden
        self.pushButton_0.clicked.connect(self.calculator.numbers)
        self.pushButton_1.clicked.connect(self.calculator.numbers)
        self.pushButton_2.clicked.connect(self.calculator.numbers)
        self.pushButton_3.clicked.connect(self.calculator.numbers)
        self.pushButton_4.clicked.connect(self.calculator.numbers)
        self.pushButton_5.clicked.connect(self.calculator.numbers)
        self.pushButton_6.clicked.connect(self.calculator.numbers)
        self.pushButton_7.clicked.connect(self.calculator.numbers)
        self.pushButton_8.clicked.connect(self.calculator.numbers)
        self.pushButton_9.clicked.connect(self.calculator.numbers)
        self.pushButton_dot.clicked.connect(self.calculator.setDecimal)
        self.pushButton_plus.clicked.connect(self.calculator.operations)
        self.pushButton_minus.clicked.connect(self.calculator.operations)
        self.pushButton_multiply.clicked.connect(self.calculator.operations)
        self.pushButton_divide.clicked.connect(self.calculator.operations)
        self.pushButton_equal.clicked.connect(self.calculator.equal)
        self.pushButton_C.clicked.connect(self.calculator.c)
        self.pushButton_CE.clicked.connect(self.calculator.ce)


    def setupThreading(self):
        """
        Diese Funktion erstellt die Threads und verknüpft die Objekte mit dem jeweiligen Thread
        """

        self.calculatorThread = QThread()
        self.calculator.moveToThread(self.calculatorThread)
        self.calculatorThread.start()

        self.calculator.send_labeltext.connect(self.label_Anzeige.setText)
        self.calculator.send_labeltext_klein.connect(self.label_Anzeige_klein.setText)


if __name__ == "__main__":
    app = QApplication(argv)
    window = AppWindow()
    exit(app.exec_())